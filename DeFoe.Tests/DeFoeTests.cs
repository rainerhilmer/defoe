﻿using System.Collections.Generic;
using System.Windows.Forms;
using Cyron43.GtaIV.Common;
using FluentAssertions;
using NUnit.Framework;

namespace DeFoe.Tests
{
   [TestFixture]
   public class DeFoeTests
   {
      [Test]
      public void Reads_the_config()
      {
         ErrorType typeOfError;
         var config = ConfigurationProvider
            .CreateOrGetThisFor(ModIdentityProvider.Identity)
            .GetConfigurationFor<ConfigurationContainer>(ModIdentityProvider.Identity, out typeOfError);
         config.AffectCops.Should().BeFalse();
         config.AutoEliminate.Should().BeFalse();
         config.JustInjure.Should().BeFalse();
         config.JustInjureOnFistAttack.Should().BeTrue();
         config.Keys.Count.Should().Be(1);
         var key = config.Keys[0];
         key.Alt.Should().BeFalse();
         key.Ctrl.Should().BeFalse();
         key.Key.Should().Be(Keys.X);
         key.Name.Should().Be("Eliminate");
         key.Shift.Should().BeFalse();
         config.Version.Should().Be(ModIdentityProvider.Identity.Version);
         typeOfError.Should().Be(ErrorType.None);
      }

      [Test, Explicit]
      public void Saves_the_config()
      {
         var keys = new List<KeyContainer>
                    {
                       new KeyContainer
                       {
                          Alt = false,
                          Ctrl = false,
                          Key = Keys.X,
                          Name = "Eliminate",
                          Shift = false
                       }
                    };
         var config = new ConfigurationContainer
                      {
                         AffectCops = false,
                         AutoEliminate = false,
                         JustInjure = false,
                         JustInjureOnFistAttack = true,
                         Keys = keys,
                         Version = ModIdentityProvider.Identity.Version
                      };
         ConfigurationProvider.SaveConfig(config, @"a:\DeFoeConfig.xml");
      }
   }
}