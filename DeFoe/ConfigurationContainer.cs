﻿using System.Collections.Generic;
using Cyron43.GtaIV.Common;

namespace DeFoe
{
   public class ConfigurationContainer : IConfigurationContainer
   {
      public bool AffectCops { get; set; }
      public bool AutoEliminate { get; set; }
      public bool JustInjure { get; set; }
      public bool JustInjureOnFistAttack { get; set; }
      public List<KeyContainer> Keys { get; set; }
      public int Version { get; set; }
   }
}