﻿using Cyron43.GtaIV.Common;
using GTA;
using KeyEventArgs = GTA.KeyEventArgs;

namespace DeFoe
{
   public class KeyHandling : Script
   {
      private readonly Core _core;

      // ReSharper disable once UnusedMember.Global
      public KeyHandling(){}

      public KeyHandling(Core core)
      {
         _core = core;
      }

      internal void HandleKey(KeyEventArgs e)
      {
         AtEliminate(e);
      }

      private void AtEliminate(KeyEventArgs e)
      {
         if(!KeyHandlingCommons.KeyIs(
            KeyHandlingCommons.GetKeyContainer(
            _core.Configuration.Keys, "Eliminate"), e))
            return;
         _core.ReadConfiguration();
         _core.FindEnemies();
      }
   }
}