﻿using System;
using System.Collections.Generic;
using System.Linq;
using Cyron43.GtaIV.Common;
using GTA;

namespace DeFoe
{
   // ReSharper disable once ClassNeverInstantiated.Global
   public class Core : Script
   {
      private readonly KeyHandling _keyHandling;
      private ErrorType _typeOfError;

      public Core()
      {
         ReadConfiguration();
         if(_typeOfError != ErrorType.None)
            return;
         _keyHandling = new KeyHandling(this);
         KeyDown += OnKeyDown;
         Interval = 250;
         if(Configuration.AutoEliminate)
            Tick += OnTick;
      }

      internal ConfigurationContainer Configuration { get; private set; }

      internal void FindEnemies()
      {
         var peds = CommonFunctions.GetPedsSafe(Player.Character.Position, 100.0f);
         foreach(var ped in peds)
         {
            if(!CommonFunctions.PedExists(ped))
               continue;
            if(!ped.isAliveAndWell
               || ped.RelationshipGroup == RelationshipGroup.Player
               || !Configuration.AffectCops && ped.PedType == PedType.Cop)
               continue;
            HandleFistAttacks(peds);
            if(ped.Weapons.Current == Weapon.Unarmed) // kills also fleeing peds if not queried.
               continue;
            if(!ped.isInMeleeCombat && !ped.isInCombat)
               continue;
            TreatEnemy(ped, false);
         }
         DetachTickEventOnConfiguration();
      }

      internal void ReadConfiguration()
      {
         Configuration = ConfigurationProvider.CreateOrGetThisFor(ModIdentityProvider.Identity)
            .GetConfigurationFor<ConfigurationContainer>(ModIdentityProvider.Identity, out _typeOfError);
      }

      private void DetachTickEventOnConfiguration()
      {
         ReadConfiguration();
         if(!Configuration.AutoEliminate)
            Tick -= OnTick;
         else
         {
            /* Attach the event but make sure it's not already attached.
             * The following is th most simple solution to guarantee that. */
            Tick -= OnTick;
            Tick += OnTick;
         }
      }

      private void HandleFistAttacks(IEnumerable<Ped> peds)
      {
         foreach(var ped in peds.Where(ped => Player.Character.HasBeenDamagedBy(ped)))
         {
            TreatEnemy(ped, true);
         }
      }

      private void OnKeyDown(object sender, KeyEventArgs e)
      {
         _keyHandling.HandleKey(e);
      }

      private void OnTick(object sender, EventArgs e)
      {
         FindEnemies();
      }

      private void TreatEnemy(Ped ped, bool bareHandsAttacked)
      {
         try
         {
            if(!ped.isAliveAndWell
               || ReferenceEquals(ped, Player.Character))
               return;
            if(bareHandsAttacked && Configuration.JustInjureOnFistAttack)
               ped.Health = -1;
            else if(Configuration.JustInjure)
               ped.Health = -1;
            else
               ped.Die();
         }
         catch(NonExistingObjectException)
         {
         }
      }
   }
}